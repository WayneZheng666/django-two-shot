from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db.models import Count


# Create your views here.
@login_required
def receipts_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt": receipt}
    return render(request, "receipts/list.html", context=context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context=context)


def redirect_homepage(request):
    return redirect("home")


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form,}
    return render(request, "receipts/create.html", context=context)


@login_required
def account_view(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context=context)


@login_required
def create_expensecategory(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expensecategory = form.save(False)
            expensecategory.owner = request.user
            expensecategory.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form,}
    return render(request, "categories/create.html", context=context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_view")
    else:
        form = AccountForm()
    context = {"form": form,}
    return render(request, "accounts/create.html", context=context)
