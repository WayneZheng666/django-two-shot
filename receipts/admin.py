from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "owner",
    ]


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "number",
        "owner",
    ]


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "vendor",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
        "total",
    ]
