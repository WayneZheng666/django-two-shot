from django.urls import path
from .views import receipts_list, create_receipt, category_list, account_view, create_expensecategory, create_account


urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_expensecategory, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("accounts/", account_view, name="account_view"),
]
